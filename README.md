# Descomplicando o Gitlab

Treinamento Descomplicando o Gitlab criado ao vivo na Twitch

### Day-1

- [x] Entendemos o que é o Git
- [x] Entendemos o que é o Gitlab
- [x] Como criar um Grupo no Gitlab
- [x] Como criar um repositório no Git
- [x] Aprendemos os comandos básicos para manipulação de arquivos e diretórios
no git
- [x] Como criar uma branch
- [x] Como criar um Merge Request
- [x] Como adicionar um Membro no projeto
- [x] Como fazer um merge na Main
- [x] Como associar um repo local com um repo remoto
- [x] Como importar um repo do GitHub para o Gitlab
- [x] Aprendemos a mudar a branch padrão















